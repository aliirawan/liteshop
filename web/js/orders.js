var app = angular.module("ordersApp",['ngDialog', 'ngFormat', 'ui.router','angularFileUpload','ui.bootstrap','croppa']);
app.config(function($stateProvider, $urlRouterProvider){
	
	$urlRouterProvider.otherwise("/list");
	
	$stateProvider
    .state('list', {
      url: "/list",
      templateUrl: "../../partials/orders/list.html",
      controller: "listCtrl"
    })
    .state('add', {
      url: "/add",
      templateUrl: "../../partials/orders/edit.html",
      controller: "addCtrl"
    })
    .state('view', {
      url: "/view/:id",
      templateUrl: "../../partials/orders/view.html",
      controller: "viewCtrl"
    });

});
app.controller('listCtrl',['$scope','$http','CroppaService',function($scope, $http, CroppaService){
	$scope.status = '';
	$scope.customer = '';
	$scope.dateOptions = {
		    formatYear: 'yyyy',
		    startingDay: 1
	};
	$scope.init = function() {
		var startDate = new Date();
		startDate.setDate(1);
	    $scope.startDate = startDate;
	    $scope.endDate = new Date();
	};
	$scope.init();
	$scope.open1 = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.opened1 = true;
	};
	$scope.open2 = function($event) {
	    $event.preventDefault();
	    $event.stopPropagation();

	    $scope.opened2 = true;
	};
	$scope.remove = function($index){
		if(confirm('Are you sure ?')){
			console.log($scope.items[$index].id);
			$http({
				method: 'delete',
				url: '/api/categories/' + $scope.items[$index].id
			})
			.then(function(response){
				//console.log(response);
				if(response.data.status=='OK'){
					$scope.items.splice($index,1);
				}
			});
		}
	}
	
	$scope.load = function(){
		$http({
			method: 'get',
			url: '/api/orders?startDate=' + $scope.startDate.toUTCString() + '&endDate=' + $scope.endDate.toUTCString() + '&status=' + $scope.status + '&customer=' + $scope.customer
		})
		.then(function(response){
			$scope.items = response.data;
		});
	};
	$scope.searchClick = function(){
		
		$scope.load();
		
	};
	$scope.load();
}]);
app.controller('viewCtrl',function($scope, $upload, CroppaService, $state, $http, $stateParams, ngDialog){
	var current_status = '';
	
	$scope.reload = function(){
		$http({
			method: 'get',
			url: '/api/orders/' + $stateParams.id
		})
		.then(function(response){
			$scope.data = response.data;
			current_status = response.data.header.status_doc;
			
			$http({
				method: 'get',
				url: '/api/customers/' + $scope.data.header.customer_id
			})
			.then(function(response){
				$scope.dynamicPopover = 'Phone 1: '+response.data.phone1;
				$scope.dynamicPopover += '\nPhone 2: '+response.data.phone2;
				
				$scope.dynamicPopoverTitle = 'Customer';
			});
			

		});
	}
	$scope.reload();
	$scope.changeStatus = function(){
		ngDialog.open({ 
			template: 'changeStatusTpl',
			controller: ['$scope',function($scopeDialog){
				$scopeDialog.new_status = current_status;
				$scopeDialog.changeStatusApply = function(){
					$http({
						method: 'post',
						url: '/api/orders/changestatus/' + $stateParams.id,
						data: {
							status: $scopeDialog.new_status
						}
					})
					.then(function(response){
						$scope.reload();
						$scopeDialog.closeThisDialog();
					});
				}
			}]
		});
	};
});
app.controller('addCtrl',function($scope, $upload, CroppaService, $state, $http){
	$scope.input = {};
	
	$scope.isUploading = false;
	$scope.input.image = '';
	$scope.progress = 0;
	$scope.onFileSelect = function($files) {
		console.log('onFileSelect');
		
		for (var i = 0; i < $files.length; i++) {
		      var file = $files[i];
		      
		      // Start upload
		      console.log(file);
		      $scope.isUploading = true;
		      $scope.upload = $upload.upload({
		          url: '/upload', //upload.php script, node.js route, or servlet url
		          //method: 'POST' or 'PUT',
		          //headers: {'header-key': 'header-value'},
		          //withCredentials: true,
		          data: {},
		          file: file, // or list of files ($files) for html5 only
		          //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
		          // customize file formData name ('Content-Disposition'), server side file variable name. 
		          //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file' 
		          // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
		          //formDataAppender: function(formData, key, val){}
		        }).progress(function(evt) {
		          $scope.progress =  parseInt(100.0 * evt.loaded / evt.total);
		          // console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
		        }).success(function(data, status, headers, config) {
		          // file is uploaded successfully
		          // console.log(data);
		          $scope.isUploading = false;
		          $scope.input.originalImage = data.name;
		          $scope.input.image = CroppaService.make('/tmp/' + data.name, 200, null);
		        });
		}
	};
	$scope.submit = function(){
		//console.log($scope.originalImage);
		
		$http({
			method: 'post',
			url: '/api/categories',
			data: $scope.input
		})
		.then(function(response){
			
			$state.go('list');
			
		});
	};
	
});
app.controller('editCtrl',function($scope, $upload, CroppaService, $state, $stateParams, $http){
	//console.log($stateParams);
	
	$http({
		method: 'get',
		url: '/api/categories/' + $stateParams.id
	})
	.then(function(response){
		// console.log(response.data);
		$scope.input = response.data;
		$scope.input.originalImage = response.data.image;
		$scope.input.image = CroppaService.make(response.data.image, 200, null);
		if(response.data.visible){
			$scope.input.visible = 'Y';
		}
	});
	
	$scope.isUploading = false;
	$scope.progress = 0;
	$scope.onFileSelect = function($files) {
		console.log('onFileSelect');
		
		for (var i = 0; i < $files.length; i++) {
		      var file = $files[i];
		      
		      // Start upload
		      console.log(file);
		      $scope.isUploading = true;
		      $scope.upload = $upload.upload({
		          url: '/upload', //upload.php script, node.js route, or servlet url
		          //method: 'POST' or 'PUT',
		          //headers: {'header-key': 'header-value'},
		          //withCredentials: true,
		          data: {},
		          file: file, // or list of files ($files) for html5 only
		          //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
		          // customize file formData name ('Content-Disposition'), server side file variable name. 
		          //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file' 
		          // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
		          //formDataAppender: function(formData, key, val){}
		        }).progress(function(evt) {
		          $scope.progress =  parseInt(100.0 * evt.loaded / evt.total);
		          // console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
		        }).success(function(data, status, headers, config) {
		          // file is uploaded successfully
		          // console.log(data);
		          $scope.isUploading = false;
		          $scope.input.originalImage = data.name;
		          $scope.input.image = CroppaService.make('/tmp/' + data.name, 200, null);
		        });
		}
	};
	$scope.submit = function(){
		//console.log($scope.input.originalImage);
		
		$http({
			method: 'put',
			url: '/api/categories/' + $stateParams.id,
			data: $scope.input
		})
		.then(function(response){
			
			$state.go('list');
			
		});
	};
});