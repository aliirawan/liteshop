var app = angular.module("categoryApp",['ui.router','angularFileUpload','ui.bootstrap','croppa']);
app.config(function($stateProvider, $urlRouterProvider){
	
	$urlRouterProvider.otherwise("/list");
	
	$stateProvider
    .state('list', {
      url: "/list",
      templateUrl: "../../partials/category/list.html",
      controller: "listCtrl"
    })
    .state('add', {
      url: "/add",
      templateUrl: "../../partials/category/add.html",
      controller: "addCtrl"
    })
    .state('edit', {
      url: "/edit/:id",
      templateUrl: "../../partials/category/edit.html",
      controller: "editCtrl"
    });

});
app.controller('listCtrl',['$scope','$http','CroppaService',function($scope, $http, CroppaService){
	
	$scope.remove = function($index){
		if(confirm('Are you sure ?')){
			console.log($scope.items[$index].id);
			$http({
				method: 'delete',
				url: '/api/categories/' + $scope.items[$index].id
			})
			.then(function(response){
				//console.log(response);
				if(response.data.status=='OK'){
					$scope.items.splice($index,1);
				}
			});
		}
	}
	$http({
		method: 'get',
		url: '/api/categories'
	})
	.then(function(response){
		$scope.items = [];
		angular.forEach(response.data, function(value, key){
			//console.log(value);
			value.image = CroppaService.make(value.image, 200, null);
			$scope.items.push(value);
		});
		
	});
	
}]);
app.controller('addCtrl',function($scope, $upload, CroppaService, $state, $http){
	$scope.input = {};
	
	$scope.isUploading = false;
	$scope.input.image = '';
	$scope.progress = 0;
	$scope.onFileSelect = function($files) {
		console.log('onFileSelect');
		
		for (var i = 0; i < $files.length; i++) {
		      var file = $files[i];
		      
		      // Start upload
		      console.log(file);
		      $scope.isUploading = true;
		      $scope.upload = $upload.upload({
		          url: '/upload', //upload.php script, node.js route, or servlet url
		          //method: 'POST' or 'PUT',
		          //headers: {'header-key': 'header-value'},
		          //withCredentials: true,
		          data: {},
		          file: file, // or list of files ($files) for html5 only
		          //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
		          // customize file formData name ('Content-Disposition'), server side file variable name. 
		          //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file' 
		          // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
		          //formDataAppender: function(formData, key, val){}
		        }).progress(function(evt) {
		          $scope.progress =  parseInt(100.0 * evt.loaded / evt.total);
		          // console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
		        }).success(function(data, status, headers, config) {
		          // file is uploaded successfully
		          // console.log(data);
		          $scope.isUploading = false;
		          $scope.input.originalImage = data.name;
		          $scope.input.image = CroppaService.make('/tmp/' + data.name, 200, null);
		        });
		}
	};
	$scope.submit = function(){
		//console.log($scope.originalImage);
		
		$http({
			method: 'post',
			url: '/api/categories',
			data: $scope.input
		})
		.then(function(response){
			
			$state.go('list');
			
		});
	};
	
});
app.controller('editCtrl',function($scope, $upload, CroppaService, $state, $stateParams, $http){
	//console.log($stateParams);
	
	$http({
		method: 'get',
		url: '/api/categories/' + $stateParams.id
	})
	.then(function(response){
		// console.log(response.data);
		$scope.input = response.data;
		$scope.input.originalImage = response.data.image;
		$scope.input.image = CroppaService.make(response.data.image, 200, null);
		if(response.data.visible){
			$scope.input.visible = 'Y';
		}
	});
	
	$scope.isUploading = false;
	$scope.progress = 0;
	$scope.onFileSelect = function($files) {
		console.log('onFileSelect');
		
		for (var i = 0; i < $files.length; i++) {
		      var file = $files[i];
		      
		      // Start upload
		      console.log(file);
		      $scope.isUploading = true;
		      $scope.upload = $upload.upload({
		          url: '/upload', //upload.php script, node.js route, or servlet url
		          //method: 'POST' or 'PUT',
		          //headers: {'header-key': 'header-value'},
		          //withCredentials: true,
		          data: {},
		          file: file, // or list of files ($files) for html5 only
		          //fileName: 'doc.jpg' or ['1.jpg', '2.jpg', ...] // to modify the name of the file(s)
		          // customize file formData name ('Content-Disposition'), server side file variable name. 
		          //fileFormDataName: myFile, //or a list of names for multiple files (html5). Default is 'file' 
		          // customize how data is added to formData. See #40#issuecomment-28612000 for sample code
		          //formDataAppender: function(formData, key, val){}
		        }).progress(function(evt) {
		          $scope.progress =  parseInt(100.0 * evt.loaded / evt.total);
		          // console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
		        }).success(function(data, status, headers, config) {
		          // file is uploaded successfully
		          // console.log(data);
		          $scope.isUploading = false;
		          $scope.input.originalImage = data.name;
		          $scope.input.image = CroppaService.make('/tmp/' + data.name, 200, null);
		        });
		}
	};
	$scope.submit = function(){
		//console.log($scope.input.originalImage);
		
		$http({
			method: 'put',
			url: '/api/categories/' + $stateParams.id,
			data: $scope.input
		})
		.then(function(response){
			
			$state.go('list');
			
		});
	};
});