angular.module('ngFormat',[])
.filter('dateTime',function(){
	return function(value){
		var date = Date.parse(value);
		// console.log(date);
		var dateObj = new Date(date);
		return dateObj.toDateString() + " " + dateObj.toLocaleTimeString();
	}
})
.filter('thousandSep',function(){
	return function(value){
		return parseInt(value).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");	
	}
	
})

