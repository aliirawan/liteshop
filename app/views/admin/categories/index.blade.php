@extends("layouts/admin")
@section("head")
	<title>Categories</title>
	<script src="/lib/angular-file-upload-shim.js"></script>
	<script src="/lib/angular.min.js"></script>
	<script src="/lib/angular-file-upload.js"></script>
	<script src="/lib/ui-bootstrap-tpls-0.11.2.min.js"></script>
	<script src="/lib/angular-croppa.min.js"></script>
	
	<script src="/lib/angular-ui-router.min.js"></script>
	<script src="/js/categories.js"></script>
@stop
@section("content")
	<div ng-app="categoryApp">
		<h2>Categories</h2>
		
		<div ui-view></div>
	</div>
	<script>
		$(document).ready(function(){
			$('#categories').addClass('active');
		});
	</script>
@stop