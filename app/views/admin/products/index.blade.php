@extends("layouts/admin")
@section("head")
	<title>Products</title>
	<script src="/lib/angular-file-upload-shim.js"></script>
	<script src="/lib/angular.min.js"></script>
	<script src="/lib/angular-animate.min.js"></script>
	<script src="/lib/angular-file-upload.js"></script>
	<script src="/lib/ui-bootstrap-tpls-0.11.2.min.js"></script>
	<script src="/lib/angular-croppa.min.js"></script>
	<script src="/lib/angular-ui-router.min.js"></script>
	
	<script src="/lib/ng-infinite-scroll.min.js"></script>
	
	<script src="/lib/ngDialog.min.js"></script>
	<link href="/css/ngDialog.min.css" rel="stylesheet" />
	<link href="/css/ngDialog-theme-default.min.css" rel="stylesheet" />
	
	<script src="/lib/angular-masonry.min.js"></script>
	<script src="/js/products.js"></script>
	
@stop
@section("content")
	<div ng-app="productApp">
		<h2>Products</h2>
		
		<div ui-view></div>
	</div>
	<script>
		$(document).ready(function(){
			$('#products').addClass('active');
		});
	</script>
	
	
@stop