<!doctype html>
<html>
	<head>
		<script src="/lib/jquery-1.11.0.min.js"></script>
		
	
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="/3rd-party/bootstrap-3.2.0-dist/css/bootstrap.min.css">
		
		<!-- Optional theme -->
		<link rel="stylesheet" href="/3rd-party/bootstrap-3.2.0-dist/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="/3rd-party/bootstrap-3.2.0-dist/js/bootstrap.min.js"></script>

		<link href="/3rd-party/font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="/css/style.css">

		@yield("head")
	
		
	</head>
	<body>
		<div id="header">
			@include("includes/nav")
		</div>
		<div class="container">
			@yield("content")
		</div>
	</body>
</html>