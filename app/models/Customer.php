<?php

class Customer extends \Eloquent {

	protected $table = 'customers';
	
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ["user_id","first_name","last_name","phone1","phone2"];

}