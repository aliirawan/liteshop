<?php

class CustomerAddress extends \Eloquent {

	protected $table = 'customers_address';
	
	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ["customer_id","address1","address2","country","state_or_province","city","postal_code"];

}