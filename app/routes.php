<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('/admin', function()
{
	return View::make('admin.home');
});
Route::resource('admin/categories', 'CategoriesController');
Route::resource('admin/products', 'ProductsController');
Route::resource('admin/orders', 'OrdersController');
Route::get('admin/logout', 'LoginController@logout');

Route::post('/upload','UploadController@doUpload');


Route::resource('/api/categories','ApiCategoryController');
Route::resource('/api/products','ApiProductController');
Route::resource('/api/orders','ApiOrdersController');
Route::resource('/api/customers','ApiCustomersController');
Route::post('/api/orders/changestatus/{id}','ApiOrdersController@changeStatus');
Route::get('/api/products/search/{page}','ApiProductController@search');
//

// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
