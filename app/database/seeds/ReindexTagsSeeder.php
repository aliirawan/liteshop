<?php


class ReindexTagsSeeder extends Seeder {

	public function run()
	{
		DB::table('tagging_tagged')->delete();
		DB::table('tagging_tags')->delete();
		
		$products = Product::leftJoin("categories","categories.id","=","products.category_id")
		               ->select( DB::raw(" products.*, categories.name as category_name") )
					   ->get();
		foreach($products as $key=>$value){
			$product = Product::find($value->id);
			// echo "->" . $value->category_name;
			$product->tag($value->name);
		}
	}
}