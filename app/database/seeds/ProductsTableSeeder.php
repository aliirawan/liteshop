<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('products')->delete();
		
		$faker = Faker::create();

		$first = Category::all()->first();
		
		foreach(range(1, 200) as $index)
		{
			if($index % 4 == 0){
				$product = Product::create([
					"name" => $faker->word,
					"description" => $faker->text,
					"image" => $faker->image('web/uploads/product', 400, 400 ),
					"visible" => true,
					"price" => 2500,
					"category_id" => $index % 10 + $first->id
				]);
				
			} else if ($index % 7 == 0 ){
				Product::create([
					"name" => $faker->word,
					"description" => $faker->text,
					"image" => $faker->image('web/uploads/product', 600, 600 ),
					"visible" => true,
					"price" => 2500,
					"category_id" => $index % 10 + $first->id
				]);
			} else {
				Product::create([
					"name" => $faker->word,
					"description" => $faker->text,
					"image" => $faker->image('web/uploads/product', 500, 500 ),
					"visible" => true,
					"price" => 2500,
					"category_id" => $index % 10 + $first->id
				]);
			}
		}
		
		DB::statement("UPDATE products SET image = substring(image from 4);");
	}

}