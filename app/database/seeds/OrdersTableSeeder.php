<?php
use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder {

	public function run()
	{
		Eloquent::unguard();
		
		DB::table('orders')->delete();
		DB::table('orders_detail')->delete();
		DB::table('orders_log_history')->delete();
		
		$faker = Faker::create();
		
		
		
		foreach(range(1, 10) as $index)
		{
			$first = Customer::orderByRaw("RANDOM()")->first();
			
			$order = Order::create([
				"doc_no" => '201411' . str_pad($index, 5, "0", STR_PAD_LEFT),
				"doc_date" => $faker->dateTimeBetween($startDate = '-3 months', $endDate = 'now'),
				"customer_id" => $first->id,
				"remark" => $faker->text,
				"status_doc" => 'N' // (N) New
			]);

			foreach(range(1, 2) as $detail_index)
			{
				$product = Product::orderByRaw("RANDOM()")->first();
				
				OrderDetail::create([
					"order_id" => $order->id,
					"product_id" => $product->id,
					"qty" => $faker->numberBetween(1, 10),
					"price" => $product->price,
				]);
			}
		}
	}
}