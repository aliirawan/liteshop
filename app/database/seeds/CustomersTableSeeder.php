<?php
use Faker\Factory as Faker;

class CustomersTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();
		DB::table('customers')->delete();
		DB::table('customers_address')->delete();
		
		$faker = Faker::create();
		
		$repo = App::make('UserRepository');
		
		$users = [];
		foreach(range(1, 10) as $index)
		{
			$input = [
			   'username' => $faker->unique()->userName,
			   'email' => $faker->unique()->email,
			   'password' => '12345',
			   'password_confirmation' => '12345'
			];
			$user = $repo->signup($input);
			$users[] = $user->id;
		}
		
		foreach(range(1, 10) as $index)
		{	
			$cust = Customer::create([
				"user_id" => array_rand($users),		
				"first_name" => $faker->firstName,
				"last_name" => $faker->lastName,
				"phone1" => $faker->phoneNumber,
				"phone2" => $faker->phoneNumber
			]);
			
			CustomerAddress::create([
				"customer_id" => $cust->id,
				"address1" => $faker->streetAddress,
				"address2" => $faker->secondaryAddress,
				"country" => "IDR",
				"state_or_province" => $faker->state,
				"city" => $faker->city,
				"postal_code" => $faker->postcode,
			]);
		}
	}
}