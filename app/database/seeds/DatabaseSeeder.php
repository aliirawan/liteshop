<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('CategoriesTableSeeder');
		$this->call('ProductsTableSeeder');
		$this->call('ReindexTagsSeeder');
		$this->call('CustomersTableSeeder');
		$this->call('OrdersTableSeeder');
	}

}
