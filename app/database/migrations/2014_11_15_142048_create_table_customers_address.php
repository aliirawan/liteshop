<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomersAddress extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers_address', function(Blueprint $table)
		{
			$table->bigIncrements('id');
			$table->bigInteger('customer_id');
			$table->string('address1');
			$table->string('address2');
			$table->string('country');
			$table->string('state_or_province');
			$table->string('city');
			$table->string('postal_code');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers_address');
	}

}
