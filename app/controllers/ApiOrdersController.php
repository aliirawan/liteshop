<?php

class ApiOrdersController extends BaseController {
	
	public function index(){
		Log::debug(Input::all());
		
		$start_date = Input::get('startDate');
		$end_date = Input::get('endDate');
		$status = Input::get('status');
		$customer = Input::get('customer');
		
		$start_date = date("Y-n-j",strtotime($start_date));
		$end_date = date("Y-n-j",strtotime($end_date));
		
		Log::debug($start_date);
		Log::debug($end_date);
		
		$orders = Order::
			 join('orders_detail','orders.id','=','orders_detail.order_id')
		     ->join('customers','customers.id','=','orders.customer_id')
		     ->select(DB::raw(' orders.id, orders.doc_no, orders.doc_date, orders.discount_amount, orders.remark, orders.status_doc, orders.customer_id, customers.first_name, customers.last_name, SUM(orders_detail.qty * orders_detail.price) as grand_total '))
			 ->where('doc_date','>=', $start_date)
			 ->where('doc_date','<=', $end_date);
		if(Input::has('status')){
			$orders = $orders->where('status_doc','=', $status);
		}
		if(Input::has('customer')){
			$orders = $orders->whereRaw("LOWER(customers.first_name) LIKE '%$customer%'");
			$orders = $orders->orWhereRaw("LOWER(customers.last_name) LIKE '%$customer%'");
		}
		$orders = $orders
		     ->groupBy(DB::raw(' orders.id, orders.doc_no, orders.doc_date, orders.discount_amount, orders.remark, orders.status_doc, orders.customer_id, customers.first_name, customers.last_name '))
		     ->orderBy('doc_date','desc')->get();
		
		return Response::json($orders);
	}
	public function show($id){
		$orders = Order::
		join('orders_detail','orders.id','=','orders_detail.order_id')
		->join('customers','customers.id','=','orders.customer_id')
		->select(DB::raw(' orders.id, orders.doc_no, orders.doc_date, orders.discount_amount, orders.remark, orders.status_doc, orders.customer_id, customers.first_name, customers.last_name, SUM(orders_detail.qty * orders_detail.price) as grand_total '))
		->groupBy(DB::raw(' orders.id, orders.doc_no, orders.doc_date, orders.discount_amount, orders.remark, orders.status_doc, orders.customer_id, customers.first_name, customers.last_name '))
		->where('orders.id','=', $id)
		->orderBy('doc_date','desc')->get();
		
		$details = OrderDetail::join('products','products.id','=','orders_detail.product_id')
					->select(DB::raw(' orders_detail.*, products.name as product_name '))
		            ->where('order_id','=', $id)->get();
		
		$log = OrderLogHistory::where("order_id","=", $id)->orderBy('created_at','desc')->get();
		
		return Response::json([
				"header" => $orders[0],
				"details" => $details,
				"log" => $log
		]);
	}
	private function getStatusText($status){
		switch($status){
			case "N": return "New Order";
			case "C": return "Waiting Customer Confirmation";
			case "O": return "On Process";
			case "W": return "Waiting Customer Payment";
			case "V": return "Void";
		}
	}
	public function changeStatus($id){
		$new_status = Input::get('status');
		
		DB::beginTransaction();
		try {
			
			$find = Order::findOrFail($id);
			$find->status_doc = $new_status;
			$find->save();
			
			// Add order log history
			$log = new OrderLogHistory();
			$log->order_id = $find->id;
			$log->message = "Change status to: " . $this->getStatusText($new_status);
			$log->save();
			
			DB::commit();
		}catch(Exception $ex){
			DB::rollback();
		}
		
		return Response::json([ "status" => "OK" ]);
	}
	
	public function store(){
		
		$object = new Category();
		$object->name = Input::get('name');
		$object->description = Input::get('description');
		$object->visible = Input::get('visible')=='Y';
		
		if(Input::has('image')){
			$image = Input::get('originalImage');
			
			Log::info("image: " . $image);
			
			$tmp = public_path('tmp');
			$cat = public_path('uploads/category');
			
			
		    copy("$tmp/$image", "$cat/$image");
		    unlink("$tmp/$image");

		    $object->image = "/uploads/category/$image";
		}
		
		$object->save();
		
		return Response::json([ "status" => "OK" ]);
	}
	public function destroy($id){
		$find = Category::findOrFail($id);
		$find->delete();
		return Response::json([ "status" => "OK" ]);
	}
	
	public function update($id){
		// return Response::json(Input::all());
		
		$object = Category::find($id);
		$object->name = Input::get('name');
		$object->description = Input::get('description');
		$object->visible = Input::get('visible')=='Y';
	
		if(Input::has('image')){
			// check if image is changed
			$image = Input::get('originalImage');
			
			Log::debug("Object->Image: $object->image");
			Log::debug("Image: $image");
			
			if($image != $object->image){
				// Remove old files
				Croppa::delete("uploads/category/" . $object->image);
				
				$tmp = public_path('tmp');
				$cat = public_path('uploads/category');
				
				copy("$tmp/$image", "$cat/$image");
				unlink("$tmp/$image");
				
				$object->image = "/uploads/category/$image";
			}
			
		}
	
		$object->save();
	
		return Response::json([ "status" => "OK" ]);
	}
}