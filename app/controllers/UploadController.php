<?php
use Webpatser\Uuid\Uuid;

class UploadController extends BaseController {
	
	
	public function doUpload(){
		$file = Input::file('file');
		
		if($file->isValid()){
			$extension = $file->getClientOriginalExtension();
			$destinationPath = public_path("tmp");
			$name = Uuid::generate(4) . ".$extension";
			
			try {
				$file->move($destinationPath, $name);
				
				return Response::json([
					"status" => "OK",
					"name" =>  $name
				]);
			}catch(Exception $e){
				return Response::json([
					"status" => "FAIL"
				]);
			}
			
			
		}
	}
}