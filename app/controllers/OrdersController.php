<?php

class OrdersController extends \BaseController {

	/**
	 * Display a listing of products
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.orders.index');
	}

}
