<?php

class ApiCategoryController extends BaseController {
	
	public function index(){
		$categories = Category::orderBy('name')->get();
		
		return Response::json($categories);
	}
	public function show($id){
		$category = Category::find($id);
		
		return Response::json($category);
	}
	
	public function store(){
		
		$object = new Category();
		$object->name = Input::get('name');
		$object->description = Input::get('description');
		$object->visible = Input::get('visible')=='Y';
		
		if(Input::has('image')){
			$image = Input::get('originalImage');
			
			Log::info("image: " . $image);
			
			$tmp = public_path('tmp');
			$cat = public_path('uploads/category');
			
			
		    copy("$tmp/$image", "$cat/$image");
		    unlink("$tmp/$image");

		    $object->image = "/uploads/category/$image";
		}
		
		$object->save();
		
		return Response::json([ "status" => "OK" ]);
	}
	public function destroy($id){
		$find = Category::findOrFail($id);
		$find->delete();
		return Response::json([ "status" => "OK" ]);
	}
	
	public function update($id){
		// return Response::json(Input::all());
		
		$object = Category::find($id);
		$object->name = Input::get('name');
		$object->description = Input::get('description');
		$object->visible = Input::get('visible')=='Y';
	
		if(Input::has('image')){
			// check if image is changed
			$image = Input::get('originalImage');
			
			Log::debug("Object->Image: $object->image");
			Log::debug("Image: $image");
			
			if($image != $object->image){
				// Remove old files
				Croppa::delete("uploads/category/" . $object->image);
				
				$tmp = public_path('tmp');
				$cat = public_path('uploads/category');
				
				copy("$tmp/$image", "$cat/$image");
				unlink("$tmp/$image");
				
				$object->image = "/uploads/category/$image";
			}
			
		}
	
		$object->save();
	
		return Response::json([ "status" => "OK" ]);
	}
}