<?php

class ApiProductController extends BaseController {
	
	public function index($page = 1){
		$pageSize = 16;
		$skip = ($page-1) * $pageSize;
		
		$products = Product::leftJoin("categories","categories.id","=","products.category_id")
		               ->select( DB::raw(" products.*, categories.name as category_name") )
		               ->orderBy('updated_at','desc')->take($pageSize)->skip($skip)->get();
		return Response::json($products);
	}
	public function show($id){
		$product = Product::find($id);
		
		return Response::json($product);
	}
	public function search($page){
		$pageSize = 16;
		$skip = ($page-1) * $pageSize;
		
		$products = Product::leftJoin("categories","categories.id","=","products.category_id")
		               ->select( DB::raw(" products.*, categories.name as category_name") )
		               ->orderBy('updated_at','desc')->take($pageSize)->skip($skip)->get();
		return Response::json($products);
	}
	
	public function store(){
		
		$object = new Product();
		$object->name = Input::get('name');
		$object->description = Input::get('description');
		$object->price = Input::get('price');
		$object->min_order = Input::get('min_order',0);
		$object->category_id = Input::get('category_id');
		$object->visible = Input::get('visible')=='Y';
		
		if(Input::has('image')){
			$image = Input::get('originalImage');
			
			Log::info("image: " . $image);
			
			$tmp = public_path('tmp');
			$cat = public_path('uploads/product');
			
			
		    copy("$tmp/$image", "$cat/$image");
		    unlink("$tmp/$image");

		    $object->image = "/uploads/product/$image";
		}
		
		$object->save();
		
		return Response::json([ "status" => "OK" ]);
	}
	public function destroy($id){
		$find = Product::findOrFail($id);
		$find->delete();
		return Response::json([ "status" => "OK" ]);
	}
	
	public function update($id){
		// return Response::json(Input::all());
		
		$object = Product::find($id);
		$object->name = Input::get('name');
		$object->description = Input::get('description');
		$object->price = Input::get('price');
		$object->min_order = Input::get('min_order',0);
		$object->category_id = Input::get('category_id');
		$object->visible = Input::get('visible')=='Y';
	
		if(Input::has('image')){
			// check if image is changed
			$image = Input::get('originalImage');
			
			Log::debug("Object->Image: $object->image");
			Log::debug("Image: $image");
			
			if($image != $object->image){
				// Remove old files
				Croppa::delete("uploads/product/" . $object->image);
				
				$tmp = public_path('tmp');
				$cat = public_path('uploads/product');
				
				copy("$tmp/$image", "$cat/$image");
				unlink("$tmp/$image");
				
				$object->image = "/uploads/product/$image";
			}
			
		}
	
		$object->save();
	
		return Response::json([ "status" => "OK" ]);
	}
}